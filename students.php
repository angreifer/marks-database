<?php
   $host = 'localhost';  // Хост, у нас все локально
    $user = 'id13291295_sanchesko';    // Имя созданного вами пользователя
    $pass = 'F5RW&>l_w3P6N}kQ'; // Установленный вами пароль пользователю
    $db_name = 'id13291295_vvid';   // Имя базы данных
    $link = mysqli_connect($host, $user, $pass, $db_name); // Соединяемся с базой
    session_start();
    if (isset($_GET['id_group'])) {
        $_SESSION['id_group'] = $_GET['id_group'];
    }
        // Ругаемся, если соединение установить не удалось
    if (!$link) {
      echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
      exit;
    }
    
    if (isset($_POST["Name"]) and $_POST["Name"] != null) {
        //Вставляем данные, подставляя их в запрос
        $Name  =  htmlspecialchars(stripslashes($_POST['Name']), ENT_QUOTES);
		$Name = str_replace("/","",$Name); 
		$Name = str_replace(".","",$Name); 
		$Name = str_replace("`","",$Name);
		
        $sql = mysqli_query($link, "INSERT INTO students (id_group, name) VALUES ('{$_SESSION['id_group']}' , '{$Name}')");
        //Если вставка прошла успешно
        header("Location: students.php");
    }
    if (isset($_GET["id_student"])) {
        //Вставляем данные, подставляя их в запрос

        $sql_data = mysqli_query($link, "SELECT DAYOFYEAR(CURDATE()) - DAYOFYEAR(deadline) as Date FROM `practical_works` WHERE id_pract = ".$_GET["id_pract"]."");
        //Если вставка прошла успешно
		$dsql = mysqli_fetch_array($sql_data);
		$data = $dsql['Date'];
		$mark = 0;
		if($data <= 0)
		{
			$mark = 5;
		}
		elseif( $data > 0 and $data <= 7) {
			$mark = 4;
		}
		else{
			$mark = 3;
		}
		
		$sql = mysqli_query($link, "INSERT INTO `connect_of_students_and_pract_works`(`id_student`, `id_pract`, `mark`) VALUES ('{$_GET["id_student"]}','{$_GET["id_pract"]}','{$mark}')");
		header("Location: students.php");
    //Если вставка прошла успешно
		
    }
?>
<!DOCTYPE html>
<html lang="ru-RU" style="background: #f2f2f2">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width" />
<title>students-DB</title>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="side_panel.js"></script>
</head>
<body>
<div id="top">
</div>
<div id="sideNav">
<div class="top_menu">
<a href="#" class="icon-menu" id="btn-menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
</div>
<ul class="colum_menu">
<li><a href="index.php" class="icon-home"><i class="fa fa-home" aria-hidden="true"></i> На главную</a></li>
<li class="srch"><i class="fa fa-search" aria-hidden="true"></i>
<form method="get" id="searchform" action="">
<input type="text" class="field" name="s" id="s" placeholder="Что будем искать?" />
<input type="submit" class="sim" name="submit"  value="" />
</form>
</li>
<li><ul class="menu">
<li><a href="tasks.php" title="Манипуляции с практическими работами" href="#">Практические работы</a></li>
<li><a href="achievements.php" title="Редактирование достижений" href="#">Достижения</a></li>
<li><a target="_blank" title="Описание пункта 3" href="#">Пункт 3</a></li>
<li><a target="_blank" title="Описание пункта 4" href="#">Пункт 4</a></li>
</ul></li>
</ul>
</div>
<form  class="flow" autocomplete="off" method="post">
<input required pattern="^[a-zA-Zа-яА-Я\s]+$" required oninvalid="this.setCustomValidity('Пожалуйста введите корректные данные о студенте')" type="text" name="Name" placeholder="Добавить студента">
<input type="submit" class="btn" value="Добавить">
</form>
<div class = "flow_table">
 <table class = "table_dark">
    <tr>
        <th>Студенты</th>
        <?php
    $a = 0;
	$my_array = array();
    $rows = mysqli_query($link, "SELECT p.id_pract,p.name_pract FROM connect_of_flows_and_pract_works AS c, practical_works AS p WHERE c.id_flow =".$_SESSION['id']." and c.id_pract = p.id_pract ORDER BY p.deadline");
    while ($stroka = mysqli_fetch_array($rows)){
        echo'<th>'.$stroka['name_pract'].'</th>';
		$my_array[$a] = $stroka['id_pract'];
		
        $a += 1;
    }
    
    echo '<th>Достижения</th>';
    
    $rows = mysqli_query($link, "SELECT * FROM students WHERE id_group=".$_SESSION['id_group']);
    while ($stroka = mysqli_fetch_array($rows)){
        echo"<tr>";
        echo"<td><a href='student_ach.php?id_student=".$stroka['id_student']."'class='flows_data'>". $stroka['name'] ."</a></td>";
        
        $rowsPract = mysqli_query($link, "SELECT c.id_pract, c.mark FROM connect_of_students_and_pract_works AS c, practical_works AS p WHERE c.id_student =".$stroka['id_student']." and c.id_pract = p.id_pract ORDER BY p.deadline");
       		
		$strokapract = mysqli_fetch_array($rowsPract);
		for ($i = 0; $i < $a ; $i++) {
			
			if( $strokapract['id_pract'] == $my_array[$i])
			{
				echo'<td>'.$strokapract['mark'].'</td>';
				$strokapract = mysqli_fetch_array($rowsPract);
			}
			else
			{
				echo'<td><a href="students.php?id_student='.$stroka['id_student'].'&id_pract='.$my_array[$i].'"role="button">click</a></td>';
			}
		}
         
        echo'<td>';
        $rowsAciv= mysqli_query($link, "SELECT a.name_achiev FROM connect_of_students_and_achivas AS c, achievements AS a WHERE c.id_student =".$stroka['id_student']." and c.id_achiev = a.id_achiev");
         while ($strokach = mysqli_fetch_array($rowsAciv))
         {
             echo '  ';
             echo $strokach['name_achiev'];
             
         }
        echo'</td>';
        echo"</tr>";
    }
    ?>
    </tr>
</table>
</div>
</body>
</html>