<?php
    $host = 'localhost';  // Хост, у нас все локально
    $user = 'id13291295_sanchesko';    // Имя созданного вами пользователя
    $pass = 'F5RW&>l_w3P6N}kQ'; // Установленный вами пароль пользователю
    $db_name = 'id13291295_vvid';   // Имя базы данных
    $link = mysqli_connect($host, $user, $pass, $db_name); // Соединяемся с базой

    // Ругаемся, если соединение установить не удалось
    if (!$link) {
      echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
      exit;
    }
    
    if (isset($_POST["Name"] )  and $_POST["Name"] != null ) {
    //Вставляем данные, подставляя их в запрос
    $sql = mysqli_query($link, "INSERT INTO flows (name_flow) VALUES ('{$_POST['Name']}')");
    //Если вставка прошла успешно
    header("Location: index.php");
    if ($sql) {
      echo '<p>Данные успешно добавлены в таблицу.</p>';
    } else {
      echo '<p>Произошла ошибка: ' . mysqli_error($link) . '</p>';
    }
    }
?>
<!DOCTYPE html>
<html lang="ru-RU" style="background: #f2f2f2">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width" />
<title>students-DB</title>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" href="googlefit.ico" type="image/x-icon">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <style>
        .removebtn {
            width: 36px; height: 36px;
            background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/trashcan.svg);
            margin-left: 1rem; border: none;
            background-position: center 4px;
            background-repeat: no-repeat;
            background-size: 25px 25px;
            opacity: .6;
            padding: 7px 17px;
            transition: .5s;
            }
        tbody tr button:hover {
            opacity: .9;
            cursor: pointer;
            }
        @keyframes hide {
            to {
           font-size: 0;
           padding: 0;
           border-bottom: none;
            }
          }
        tr.hidden td {
            animation: hide 1s 1s forwards;
            }
        tr.hidden {
            opacity: 0;
        }
        #zen tr { transition: .8s opacity; }
    </style>
<script src="side_panel.js"></script>
</head>
<body>
<div id="top">
</div>
<div id="sideNav">
<div class="top_menu">
<a href="#" class="icon-menu" id="btn-menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
</div>
<ul class="colum_menu">
<li><a href="index.php" class="icon-home"><i class="fa fa-home" aria-hidden="true"></i> На главную</a></li>
<li class="srch"><i class="fa fa-search" aria-hidden="true"></i>
<form method="get" id="searchform" action="">
<input type="text" class="field" name="s" id="s" placeholder="Что будем искать?" />
<input type="submit" class="sim" name="submit"  value="" />
</form>
</li>
<li><ul class="menu">
<li><a href="tasks.php" title="Манипуляции с практическими работами" href="#">Практические работы</a></li>
<li><a href="achievements.php" title="Редактирование достижений" href="#">Достижения</a></li>
<li><a target="_blank" title="Описание пункта 3" href="#">Пункт 3</a></li>
<li><a target="_blank" title="Описание пункта 4" href="#">Пункт 4</a></li>
</ul></li>
</ul>
</div>
<form  class="flow" autocomplete="off" method="post">
<input type="text" name="Name" placeholder="Добавить поток">
<input type="submit" class="btn" value="Добавить">
</form>
<div class = "flow_table">
 <table id="zen" class = "table_dark">
    <tr>
        <th>Потоки</th>
        <th>Удаление</th>
   <?php
    $rows = mysqli_query($link, "SELECT * FROM flows");
    while ($stroka = mysqli_fetch_array($rows)){
        echo"<tr>";
        echo'<td><a class="flows_data" href="groups.php?id='.$stroka['id_flow'].'">'.$stroka['name_flow'] .'</a></td>';
        echo"</tr>";
    }
    ?>
    </tr>
</table>
<script>
    let procRows = zen.querySelectorAll("tbody tr");
    for (let i = 1; i < procRows.length; i++) 
    {
        procRows[i].innerHTML += '<td><button class="removebtn" title="Remove"></td>';
    }
</script>
<script>
    zen.querySelector("tbody").addEventListener("click", function(e) {
    if (e.target.nodeName == "BUTTON") 
    {
        let cell = e.target.parentNode; 
        cell.parentNode.classList.add("hidden");
        e.target.remove();
    }
    })
</script>
</div>
</body>
</html>