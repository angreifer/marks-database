<?php
    $host = 'localhost';  // Хост, у нас все локально
    $user = 'id13291295_sanchesko';    // Имя созданного вами пользователя
    $pass = 'F5RW&>l_w3P6N}kQ'; // Установленный вами пароль пользователю
    $db_name = 'id13291295_vvid';   // Имя базы данных
    $link = mysqli_connect($host, $user, $pass, $db_name); // Соединяемся с базой
    session_start();
    if (isset($_GET['id'])) {
        $_SESSION['id'] = $_GET['id'];
    }
        // Ругаемся, если соединение установить не удалось
    if (!$link) {
      echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
      exit;
    }
    
    if (isset($_POST["Name"]) and $_POST["Name"] != null) {
        //Вставляем данные, подставляя их в запрос
        $sql = mysqli_query($link, "INSERT INTO groups (id_flow, name_group) VALUES ('{$_SESSION['id']}' , '{$_POST['Name']}')");
        //Если вставка прошла успешно
        header("Location: groups.php");
    }
    
    if (isset($_POST["Name_pract"])  and $_POST["Name_pract"] != null ){
    //Вставляем данные, подставляя их в запрос
    $sql = mysqli_query($link, "INSERT INTO connect_of_flows_and_pract_works (id_flow, id_pract) VALUES ('{$_SESSION['id']}','{$_POST['Name_pract']}')");
    //Если вставка прошла успешно
    header("Location: groups.php");
    if ($sql) {
      echo '<p>Данные успешно добавлены в таблицу.</p>';
    } else {
      echo '<p>Произошла ошибка: ' . mysqli_error($link) . '</p>';
    }
    }


?>
<!DOCTYPE html>
<html lang="ru-RU" style="background: #f2f2f2">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width" />
<title>students-DB</title>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" href="googlefit.ico" type="image/x-icon">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="side_panel.js"></script>
</head>
<body>
<div id="top">
</div>
<div id="sideNav">
<div class="top_menu">
<a href="#" class="icon-menu" id="btn-menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
</div>
<ul class="colum_menu">
<li><a href="index.php" class="icon-home"><i class="fa fa-home" aria-hidden="true"></i> На главную</a></li>
<li class="srch"><i class="fa fa-search" aria-hidden="true"></i>
<form method="get" id="searchform" action="">
<input type="text" class="field" name="s" id="s" placeholder="Что будем искать?" />
<input type="submit" class="sim" name="submit"  value="" />
</form>
</li>
<li><ul class="menu">
<li><a href="tasks.php" title="Манипуляции с практическими работами" href="#">Практические работы</a></li>
<li><a href="achievements.php" title="Редактирование достижений" href="#">Достижения</a></li>
<li><a target="_blank" title="Описание пункта 3" href="#">Пункт 3</a></li>
<li><a target="_blank" title="Описание пункта 4" href="#">Пункт 4</a></li>
</ul></li>
</ul>
</div>
<form  class="flow" autocomplete="off" method="post">
    <input type="text" name="Name" placeholder="Добавить группу">
    <input type="submit" class="btn" value="Добавить">
</form>

    
<form  class= "telo"  method="POST">
    <select name="Name_pract">
        <?php
        $rows = mysqli_query($link, "SELECT * FROM practical_works ORDER BY name_pract");
        while ($stroka = mysqli_fetch_array($rows)){
            echo'<option value="'.$stroka['id_pract'].'">'.$stroka['name_pract'].'</option>';
        }
        ?>
    </select>
    <input type="submit" class="btn" value="Добавить">
</form> 


    
<div class = "flow_table">
 <table class = "table_dark">
    <tr>
        <th>Группы</th>
    <?php
    
    $rows = mysqli_query($link, "SELECT * FROM groups WHERE id_flow =".$_SESSION['id']." ORDER BY name_group");
    while ($stroka = mysqli_fetch_array($rows)){
        echo"<tr>";
        echo'<td><a class="flows_data" href="students.php?id_group='.$stroka['id_group'].'">'.$stroka['name_group'] .'</a></td>';
        echo"</tr>";
    }
    ?>
    </tr>
</table>
</div>
<div>
 <table style="position: fixed; top: 20%; left: 50%;" class = "table_dark">
    <tr>
        <th>Практические работы</th>
        <th>Дедлайн</th>
    <?php
    $rows = mysqli_query($link, "SELECT p.name_pract, p.deadline FROM connect_of_flows_and_pract_works AS c, practical_works AS p WHERE c.id_flow =".$_SESSION['id']." and c.id_pract = p.id_pract");
    while ($stroka = mysqli_fetch_array($rows)){
        echo"<tr>";
        echo'<td>'.$stroka['name_pract'].'</td>';
        echo'<td>'.$stroka['deadline'].'</td>';
        echo"</tr>";
    }
    ?>
    </tr>
</table>
</div>
</body>
</html>